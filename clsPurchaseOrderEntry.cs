﻿using MonitorBase.Dll;
using MonitorBase.InterfaseIntermedia;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ServiceModel.Channels;
using Epicor.ServiceModel.StandardBindings;
using Erp.Proxy.BO;
using Erp.BO;

namespace MonitorBase.PurchaseOrderEntry
{
    public class clsPurchaseOrderEntry : IMonitorBase
    {
        public int SubJobID;//= 25719;
        public SqlConnection con;
        public GlobalMembers GlbGM;

        string appServerUrl;// "net.tcp://10.10.161.30/E10Prod";
        string Username;//= "jcenteno";
        string Password;//= "developer94";

        //tabla para llenar los datos
        public DataTable POHeader;
        public DataTable PODetail;
        public DataTable PORel;
        public DataTable POHeaderData;
        /// <summary>
        /// método validar del monitor 
        /// </summary>
        /// <returns></returns>
        public bool StartValidation()
        {
            bool result = false;
            SqlCommand command = new SqlCommand("[Int_StartValidation]", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
            result = Convert.ToBoolean(dt.Rows[0]["Start"].ToString());
            return result;
        }

        /// <summary>
        /// Método para obtener datos de la bd 
        /// </summary>
        /// <returns></returns>
        public bool GetDatos()
        {
            ClaseGetDatos classGet = new ClaseGetDatos();
            classGet.SubJobID = SubJobID;
            bool result = false;
            string error = "";
            POHeader = classGet.GetDatable(con, "Int_GetDatosPOHeader", "POHeader", out result, out error);
            if (result == false) { return false; }
            return true;
        }


        public GlobalMembers Execute(GlobalMembers globalMembers)
        {
            GlbGM = globalMembers;

            GlbGM.MessageStatus = true;
            SubJobID = GlbGM.SubJobId;
            con = GlbGM.getConexionSql();

            appServerUrl =GlbGM.getConexionEpicor().Servidor; //"10.10.161.30";
            Username = GlbGM.getConexionEpicor().User;
            Password = GlbGM.getConexionEpicor().Password;

            if (!StartValidation())
            {
                GlbGM.MessageStatus = false;
                GlbGM.MessageError = "No inicio, por StartValidation";
                return GlbGM;

            }
            else
            {
                SaveStatus("Running");

                if (GetDatos())
                {
                    Process();
                }
                else
                {
                    GlbGM.MessageStatus = false;
                    GlbGM.MessageError = "Error obteniendo datos";
                    return GlbGM;
                }

                return globalMembers;
            }
        }

        public void Process()
        {
            try
            {

                DataTable datablet = POHeader;
                DataRow dt = datablet.Rows[0];
                string Company = dt["Company"].ToString();
                string Plant = dt["Plant"].ToString();
                string TaxRegionCode = dt["TaxRegionCode"].ToString(); //datos para validar 
                CustomBinding wcfBindingU = NetTcp.UsernameWindowsChannel();
                Uri uriUserFile = new Uri(String.Format(@"{0}/Ice/BO/{1}.svc", appServerUrl, "UserFile"));
                Ice.Proxy.BO.UserFileImpl oUserFile = new Ice.Proxy.BO.UserFileImpl(wcfBindingU, uriUserFile);
                oUserFile.ClientCredentials.UserName.UserName = Username;
                oUserFile.ClientCredentials.UserName.Password = Password;
                //configuración para entrar directo en una compañia y la planta 
                oUserFile.SaveSettings(Username, true, Company, true, false, true, true, true, true, true, true, true,
                                       false, false, -2, 0, 1456, 886, 2, "MAINMENU", Plant, "", 0, -1, 0, "", false, "Default");

                CustomBinding wcfBinding = NetTcp.UsernameWindowsChannel();
                string partName = "PO";
                Uri partUri = new Uri(String.Format(@"{0}/Erp/BO/{1}.svc", appServerUrl, partName));

                using (var POImpl = new POImpl(wcfBinding, partUri))
                {
                    PODataSet PODataSet = new PODataSet();
                    POHeaderListDataSet pOHeaderListDataSet = new POHeaderListDataSet();
                    POImpl.ClientCredentials.UserName.UserName = Username;
                    POImpl.ClientCredentials.UserName.Password = Password;

                    int existeDato = Convert.ToInt32(POHeader.Rows[0]["existe"].ToString());
                    int ponumvalida = Convert.ToInt32(dt["PONumEpic"].ToString());

                    if (existeDato == 1)
                    {
                        PODataSet = POImpl.GetByID(ponumvalida);                       
                        PODataSet.Tables["POHeader"].Rows[0].BeginEdit();
                        PODataSet.Tables["POHeader"].Rows[0].Delete();
                        PODataSet.Tables["POHeader"].Rows[0].EndEdit();
                        POImpl.Update(PODataSet);

                    }


                    //Crea un nuevo POHeader  
                    POImpl.GetNewPOHeader(PODataSet);
                   // PODataSet.Tables["POHeader"].Rows[0]["Company"] = dt["Company"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["EntryPerson"] = dt["EntryPerson"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["OrderDate"] = dt["OrderDate"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipViaCode"] = dt["ShipViaCode"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["TermsCode"] = dt["TermsCode"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipName"] = dt["ShipName"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipAddress1"] = dt["ShipAddress1"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipAddress2"] = dt["ShipAddress2"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipAddress3"] = dt["ShipAddress3"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipCity"] = dt["ShipCity"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipState"] = dt["ShipState"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["ShipZIP"] = dt["ShipZIP"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["BuyerID"] = dt["BuyerID"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["VendorNum"] = dt["VendorNum"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["DueDate"] = dt["DueDate"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["BinNum_c"] = dt["BinNum"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["WarehouseCode_c"] = dt["WarehouseCode_c"].ToString();
                    PODataSet.Tables["POHeader"].Rows[0]["CommentText"] = "PONumI*" + dt["PONumIntegration"].ToString();
                    string VendorID = dt["VendorID"].ToString();

                    POImpl.ChangeVendor(VendorID, PODataSet);
                    //Actualiza el Dataset de Epicor 
                    POImpl.Update(PODataSet);

                    string responseCompany = PODataSet.Tables["POHeader"].Rows[0]["Company"].ToString();
                    string responsePonum = PODataSet.Tables["POHeader"].Rows[0]["PONum"].ToString();
                    string responseSysRowID = PODataSet.Tables["POHeader"].Rows[0]["SysRowID"].ToString();
                    string responseBuyerID = PODataSet.Tables["POHeader"].Rows[0]["BuyerID"].ToString();
                    string responseCommentText = PODataSet.Tables["POHeader"].Rows[0]["CommentText"].ToString();
                    string responseTaxRegioncode = PODataSet.Tables["POHeader"].Rows[0]["TaxRegioncode"].ToString();

                    string POHeaderIDval = dt["POHeaderID"].ToString();

                    // Query para guardar el PONum y el SysRowid
                    SaveReference(responseSysRowID, "POHeader");
                    using (SqlCommand command = con.CreateCommand())
                    {
                        con.Open();
                        command.CommandText = "UPDATE Int_POHeader SET PONum = '" + responsePonum + "' Where Company = '" + responseCompany + "' and SysRowid ='" + responseSysRowID + "'";
                        command.ExecuteNonQuery();
                    }

                    ClaseGetDatos classGet = new ClaseGetDatos();
                    bool result = false;
                    string error = "";
                    //Query para consultar los datos de la tabla PODetail por la condición de id POHeaderID
                    PODetail = classGet.GetDatableString(con, "Select * from Int_PODetail  where POHeaderID = '" + POHeaderIDval + "'order by POLine asc", out result, out error);


                    DataTable Deatail = PODetail;
                    int i = 0;
                    foreach (DataRow dtDeatail in Deatail.Rows)
                    {
                        POImpl.GetNewPODetail(PODataSet, Convert.ToInt32(responsePonum));

                        string respNewPartNum = dtDeatail["PartNum"].ToString();
                        string respNewPlant = dtDeatail["Plant"].ToString();
                        decimal respCalcOurQty = Convert.ToDecimal(dtDeatail["OrderQty"]);

                        bool isSubstitute = false;//varibale por default de epicor
                        string rowType = "";//varibale por default de epicor
                        Guid SysRowID = Guid.Empty;//varibale por default de epicor
                        bool multipleMatch = false;//varibale por default de epicor

                        //Método ChangeDetailPartNum recupera información del PartNum                            
                        POImpl.ChangeDetailPartNum(ref respNewPartNum, SysRowID, rowType, isSubstitute, out multipleMatch, PODataSet);
                        PODataSet.Tables["PODetail"].Rows[i]["UnitCost"] = dtDeatail["UnitCost"].ToString();
                        PODataSet.Tables["PODetail"].Rows[i]["PUM"] = dtDeatail["PUM"].ToString();
                        PODataSet.Tables["PODetail"].Rows[i]["DueDate"] = dtDeatail["DueDate"].ToString();
                        PODataSet.Tables["PODetail"].Rows[i]["QtyOption"] = dtDeatail["QtyOption"].ToString();
                        PODataSet.Tables["PODetail"].Rows[i]["TaxCatID"] = dtDeatail["TaxCatID"].ToString();
                        PODataSet.Tables["PODetail"].Rows[i]["Taxable"] = dtDeatail["Taxable"].ToString();
                        


                        //Método ChangeDetailCalcVendQty realizar el cálculo de la orden 
                        POImpl.ChangeDetailCalcVendQty(respCalcOurQty, PODataSet);

                        string cOrderChangedMsgText1 = "";//varibale por default de epicor
                        string taxableChangedMsgText1 = "";//varibale por default de epicor
                        string vendorChangedMsgText1 = "";//varibale por default de epicor
                        string viewName1 = "PODetail";//varibale por default de epicor

                        POImpl.CheckBeforeUpdate(out cOrderChangedMsgText1, out taxableChangedMsgText1, out vendorChangedMsgText1, viewName1, PODataSet);
                        POImpl.Update(PODataSet);

                        string PODetailDval = dtDeatail["PODetailID"].ToString();
                        //Query para obtener los datos de PORel
                        PORel = classGet.GetDatableString(con, "Select * FROM Int_PORel   where PODetailID = '" + PODetailDval + "'", out result, out error);
                        DataTable Release = PORel;

                        POImpl.ChangeRelPlant(respNewPlant, PODataSet);
                        PODataSet.Tables["PORel"].Rows[0]["WarehouseCode"] = Release.Rows[0]["WarehouseCode"].ToString();
                        PODataSet.Tables["PORel"].Rows[0]["Plant"] = Release.Rows[0]["Plant"].ToString();
                        PODataSet.Tables["PORel"].Rows[0]["Taxable"] = Release.Rows[0]["Taxable"].ToString();
                        
                        POImpl.Update(PODataSet);

                        i++;

                    }

                    POHeaderData = GetData(con, responseCompany, Convert.ToInt32(responsePonum), "Int_GetDatosPODetail", out result, out error);
                    int valido = Convert.ToInt32(POHeaderData.Rows[0]["valido"].ToString());
                    string mensaje = POHeaderData.Rows[0]["mensaje"].ToString();

                    if (valido == 1)
                    {
                        Console.WriteLine("Todo los datos son correctos y se aprueba una orden");
                        bool aceptar = true;//varibale por default de epicor
                        string violationMsg = "";//varibale por default de epicor
                        PODataSet.Tables["POHeader"].Rows[0]["RowMod"] = "U";
                        POImpl.ChangeApproveSwitch(aceptar, out violationMsg, PODataSet);
                        string cOrderChangedMsgText = "";//varibale por default de epicor
                        string taxableChangedMsgText = "";//varibale por default de epicor
                        string vendorChangedMsgText = "";//varibale por default de epicor
                        string viewName = "POHeader"; //varibale por default de epicor
                        POImpl.CheckBeforeUpdate(out cOrderChangedMsgText, out taxableChangedMsgText, out vendorChangedMsgText, viewName, PODataSet);
                        //Actualiza el Dataset de Epicor 
                        POImpl.Update(PODataSet);
                        POImpl.ChangedApproveSwitch(Convert.ToInt32(responsePonum), PODataSet, out violationMsg);

                        SaveStatus("Finished");
                        SavePOEstado("Int_GetDatosPOHeader", "UpdateSta");
                        GlbGM.MessageStatus = true;
                        GlbGM.MessageError = "";

                    }
                    else
                    {
                        Console.WriteLine("Fallo algun dato y no se pudo crear una orden");
                        PODataSet POData = new PODataSet();
                        POData = POImpl.GetByID(Convert.ToInt32(responsePonum));
                        if (POData != null)
                        {
                            POData.Tables["POHeader"].Rows[0].BeginEdit();
                            POData.Tables["POHeader"].Rows[0].Delete();
                            POData.Tables["POHeader"].Rows[0].EndEdit();
                            POImpl.Update(POData);

                        }
                        throw new Exception("Fallo algun dato y no se pudo crear una orden");
                    }
                }
            }
            catch (Exception ex)
            {
                SaveStatus("Error");
                GlbGM.MessageStatus = false;
                GlbGM.MessageError = "Process: " + ex.ToString();
                SaveError(ex.ToString());
            }
        }
        public DataTable GetData(SqlConnection Con, string Company, int POnum, string Table, out bool result, out string Error)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlConnection connection = Con;
                SqlCommand command = new SqlCommand(Table, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Company", Company);
                command.Parameters.AddWithValue("@PONum", POnum);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                result = true;
                Error = "";
                return ds;
            }
            catch (Exception ex)
            {
                result = false;
                Error = ex.Message.ToString();
                return ds;
            }

        }
        public void SavePOEstado(string procedure, string Tipo)
        {
            SqlCommand command = new SqlCommand(procedure, con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);
            command.Parameters.AddWithValue("@Tipo", Tipo);

            DataTable ds = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
        }
        public void SaveStatus(string Status)
        {
            SqlCommand command = new SqlCommand("[Int_SaveStatus]", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);
            command.Parameters.AddWithValue("@Status", Status);

            DataTable ds = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
        }
        public void SaveError(string MsgError)
        {
            SqlCommand command = new SqlCommand("[Int_SaveError]", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);
            command.Parameters.AddWithValue("@MsgError", MsgError);

            DataTable ds = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
        }
        public void SaveReference(string Reference, string Table)
        {
            SqlCommand command = new SqlCommand("[Int_SaveReference]", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);
            command.Parameters.AddWithValue("@Reference", Reference);
            command.Parameters.AddWithValue("@Table", Table);

            DataTable ds = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
        }
    }

    public class ClaseGetDatos
    {
        public int SubJobID { get; set; }

        // se inserta un tabla dinamicamnte al procedure de int_GetDatos
        public DataTable GetDatable(SqlConnection Con, string procedure, string Tipo, out bool result, out string Error)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlConnection connection = Con;
                SqlCommand command = new SqlCommand(procedure, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SubJobID", SubJobID);
                command.Parameters.AddWithValue("@Tipo", Tipo);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                result = true;
                Error = "";
                return ds;

            }
            catch (Exception ex)
            {
                result = false;
                Error = ex.Message.ToString();
                return ds;
            }

        }

        public DataTable GetDatableString(SqlConnection Con, string StringSQL, out bool result, out string Error)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlConnection connection = Con;
                SqlCommand command = new SqlCommand(StringSQL, connection);
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);

                result = true;
                Error = "";
                return ds;

            }
            catch (Exception ex)
            {
                result = false;
                Error = ex.Message.ToString();
                return ds;
            }

        }

    }
}
